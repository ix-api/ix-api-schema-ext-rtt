"""
IX-API extension: RTT Statistics
"""

import json
from inspect import cleandoc

def response(code, res):
    """Make a json success response"""
    return {
        code: {
            "description": res["description"],
            "content": {
                "application/json": {
                    "schema": res["schema"],
                },
            },
        },
    }


def query_param(name, schema=None, **kwargs):
    """Make query param"""
    if not schema:
        schema = {
            "type": "string",
        }

    return {
        "name": name,
        "schema": schema,
        "in": "query",
        "description": f"Filter by {name}",
        "required": False,
        **kwargs,
    }


EXT_DESCRIPTION = """
This extension to the IX-API provides opeations to query
and subscribe to `RTT` information about peers consuming a `NetworkService`.
"""

EXT_NOT_FOUND = {
    "description": "NotFound",
    "schema": {
        "$ref": "ix-api-schema-2.4.1.json#/components/schemas/ProblemResponse",
    }
}

EXT_PEER_RTT = {
    "title": "PeerRTT",
    "type": "object",
    "description": "PeerRTT",
    "properties": {
        "serial": {
            "type": "integer",
            "minimum": 0,
            "example": 42834781723,
            "description": cleandoc("""
                The `serial` is an incrementing counter. You can use it
                to poll for changes.
            """),
        },
        "timestamp": {
            "type": "string",
            "format": "date-time",
            "description": cleandoc("""
                The date and time when the RTT statistic was measured.
            """),
        },
        "neighbor": {
            "type": "string",
            "example": "Fast Packets LLC.",
            "description": cleandoc("""
                Name of the peer.
            """),
        },
        "asn": {
            "type": "integer",
            "example": 42000001,
            "description": cleandoc("""
                ASN of the peer.
            """),
        },
        "ip": {
            "type": "string",
            "example": "fd42:1000::1",
            "description": cleandoc("""
                The IP address of the peer.
            """),
        },
        "time_ms": {
            "type": "integer",
            "minimum": 0,
            "example": 16000,
            "description": cleandoc("""
                The total duration of the measurement.
            """),
        },
        "tx": {
            "type": "integer",
            "minimum": 0,
            "example": 10,
            "description": cleandoc("""
                The number of *packets transmitted*.
            """),
        },
        "rx": {
            "type": "integer",
            "minimum": 0,
            "example": 10,
            "description": cleandoc("""
                The number of *packets received*.
            """),
        },
        "loss": {
            "type": "float",
            "minimum": 0.0,
            "maximum": 1.0,
            "example": 0.0,
            "description": cleandoc("""
                Packet loss as `rx` / `tx` ratio.
            """),
        },
        "rtt_min_ms": {
            "type": "float",
            "example": 0.493,
            "description": cleandoc("""
                Minimum RTT in milliseconds.
            """),
        },
        "rtt_avg_ms": {
            "type": "float",
            "example": 0.523,
            "description": cleandoc("""
                Average RTT in milliseconds.
            """),
        },
        "rtt_max_ms": {
            "type": "float",
            "example": 0.610,
            "description": cleandoc("""
                Maximum RTT in milliseconds.
            """),
        },
        "rtt_mdev_ms": {
            "type": "float",
            "example": 0.012,
            "description": cleandoc("""
                Standard deviation in milliseconds.
            """),
        },
    },
    "required": [
        "serial",
        "timestamp",
        "neighbor",
        "asn",
        "ip",
        "time_ms",
        "tx",
        "rx",
        "loss",
        "rtt_min_ms",
        "rtt_max_ms",
        "rtt_avg_ms",
        "rtt_mdev_ms",
    ],
}

EXT_NETWORK_FEATURE_RTT = {
    "title": "RttNetworkFeature",
    "type": "object",
    "description": "RttNetworkFeature",
    "properties": {
        "type": {
            "type": "string",
            "enum": ["rtt_statistics_policy", "route_server"],
        },
        "required": {
            "type": "boolean",
            "description": cleandoc("""
                Some exchanges need an explicit policy on wether
                or not to share RTT statistics.

                If `true` configuring a policy is mandatory.
            """),
        },
        "network_service": {
            "type": "string",
            "description": cleandoc("""
                The ID of the `NetworkService` this policy applies to.
            """)
        },
    },
    "required": [
        "type",
        "required",
        "network_service",
    ],
}

EXT_NETWORK_FEATURE_CONFIG_RTT = {
    "title": "RttNetworkFeatureConfig",
    "type": "object",
    "description": "RttNetworkFeatureConfig",
    "properties": {
        "id": {
            "type": "string",
            "description": cleandoc("""The ID of the NetworkFeatureConfig"""),
            "readOnly": True,
        },
        "type": {
            "type": "string",
            "enum": ["rtt_statistics_policy", "route_server"],
        },
        "network_feature": {
            "type": "string",
            "description": cleandoc("""
                The ID of the `NetworkFeature` this
                configuration applies to.
            """),
        },
        "network_service_config": {
            "type": "string",
            "description": cleandoc("""
                ID of the `NetworkServiceConfig` this policy
                applies to.
            """),
        },
        "policy": {
            "type": "string",
            "enum": ["allow", "deny"],
            "description": cleandoc("""
                ### allow

                Choosing `allow` will grant access to RTT measurements on the
                platform:

                If `consuming_account` is left blank or `null` it is treated
                as a wildcard and all accounts can access your RTT statistics.
                Otherwise access is granted to specific accounts only.

                If no *wildcard policy* is defined, it is assumed that access
                is selectivly granted to these accounts and denied to all other
                on the platform.

                ### deny

                Choosing `deny` will prevent access to your RTT statistics.

                If `consuming_account`  is left blank or `null` it will be
                treated as a wildcard and no account will be able to access RTT
                statistics.

                You can then selectively grant accesss by creating additional
                `rtt_statistics_policy` **NetworkFeatureConfigs** with an `allow`
                policy for specific `consuming_account`s.

                If no *wildcard policy* is defined, it is assumed that any
                other account on the platform can access RTT statistics, unless
                an explicit `deny` policy exists.

            """),
       },
        "consuming_account": {
            "type": "string",
            "nullable": True,
            "description": cleandoc("""
                The id of the account the policy applies to. It will either
                be granted access or will be explicitly excluded from accessing
                your RTT data.

                If left blank or set to `null` it will be treated as a
                *wildcard*.
            """),
        },
        "managing_account": {
            "type": "string",
            "description": cleandoc("""
                The id of the account responsible for managing the service via
                the API. A manager can read and update the state of entities.
            """)
        },
    },
    "required": [
        "id",
        "type",
        "network_feature",
        "network_service_config",
        "policy",
        "managing_account",
    ],
}



EXT_COMPONENTS = {
    "schemas": {
        "PeerRTT": EXT_PEER_RTT,
        "RttNetworkFeature": EXT_NETWORK_FEATURE_RTT,
        "RttNetworkFeatureConfig": EXT_NETWORK_FEATURE_CONFIG_RTT,
    },
}


EXT_RTT_READ_SUCCESS = {
    "description": "List of: PeerRTT",
    "schema": {
        "type": "array",
        "items": {
            "$ref": "#/components/schemas/PeerRTT",
        },
    },
}


EVENTS_EXAMPLE_SHORT = """
data: {"serial": 421233, "asn": 42000001, "ip": "fd42:1000::1", ...}
id: 421233

data: {"serial": 421234, "asn": 42000002, "ip": "fd42:1000::1", ...}
id: 421234

"""

EVENTS_SSE_BODY = """
A stream of Server Sent Events is returned.
The measurement result is encoded as JSON.

### Example:

```
data: {"serial": 42834781723, "timestamp": "2019-08-24T14:15:22Z", "asn": 42000001, ...}
id: 42834781723

data: {"serial": 42834781724, "timestamp": "2019-08-24T14:16:22Z", "asn": 42000002, ...}
id: 42834781724

...
```

For more information on how to use server sent events, see:
https://html.spec.whatwg.org/multipage/server-sent-events.html#server-sent-events

"""

EXT_NS_STATISTICS_RTT_READ = {
    "get": {
        "operationId": "network_services_statistics_rtt_read",
        "description": cleandoc("""
            Get **rtt statistics** from neighbors consuming this network
            service. You can filter by `asn`, peer `ip` or `neighbor`.

            If the `NetworkService` does not support RTT statistics,
            a `404` error response will be returned.

            ### Receiving Updates
            To poll for updates, you can provide a `serial` to the
            `after` parameter. All of the above filters can be applied
            here and allow for receiving updates for a specific peer.

            If no `serial` is provided the latest RTT statistics are returned.

            ### Event Streaming
            This endpoint supports streaming updates using
            [**Server Sent Events**](https://html.spec.whatwg.org/multipage/server-sent-events.html#server-sent-events).

            To subscribe to events, negotiate the response content
            using the HTTP header: `Accept: text/event-stream`.

            Filteres are applied as above.
        """),
        "tags": ["network-services"],
        "produces": ["text/event-stream","application/json"],
        "responses": {
            "200": {
                "description": EXT_RTT_READ_SUCCESS["description"],
                "content": {
                    "application/json": {
                        "schema": EXT_RTT_READ_SUCCESS["schema"],
                    },
                    "text/event-stream": {
                        "schema": {
                            "type": "string",
                            "description": EVENTS_SSE_BODY,
                        },
                        "examples": {
                            "streamExample": {
                                "value": EVENTS_EXAMPLE_SHORT,
                            },
                        },
                    },
                },
            },
            **response("404", EXT_NOT_FOUND),
        },
        "parameters": [
            query_param("asn", description=cleandoc("""
                Show only results from this `asn`.
            """)),
            query_param("neighbor", description=cleandoc("""
                Show only results from this `neighbor`.
            """)),
            query_param("ip", description=cleandoc("""
                Show only results from this neighbor `ip` address.
            """)),
            query_param("after", description=cleandoc("""
                Show only results with a `serial` greater than `after`.
            """)),
        ],
    }
}



EXT_NF_LIST_SUCCESS = {
    "description": "List of: NetworkFeature",
    "schema": {
        "type": "array",
        "items": {
            "$ref": "#/components/schemas/RttNetworkFeature",
        },
    },
}

EXT_NF_READ_SUCCESS = {
    "description": "NetworkFeature",
    "schema": {
        "$ref": "#/components/schemas/RttNetworkFeature",
    },
}

EXT_NF_LIST = {
    "get": {
        "operationId": "network_features_list",
        "description": cleandoc("""
            A NetworkFeature represents additional funcationality of a single
            NetworkService.

            For certain NetworkFeatures, which are marked as required, one
            `NetworkFeatureConfig` needs to be created in order to move the
            `NetworkServiceConfig` into production.
        """),
        "tags": ["network-features"],
        "responses": {
            **response("200", EXT_NF_LIST_SUCCESS),
        },
    },
}

EXT_NF_READ = {
    "get": {
        "operationId": "network_features_read",
        "description": cleandoc("""
            Retreive a `NetworkFeature` by ID.
        """),
        "tags": ["network-features"],
        "responses": {
            **response("200", EXT_NF_READ_SUCCESS),
        },
    },
}

EXT_NFC_CREATE_SUCCESS = {
    "description": "NetworkFeatureConfig",
    "schema": {
        "$ref": "#/components/schemas/RttNetworkFeatureConfig",
    },
}

EXT_NFC_COLLECTION = {
    "post": {
        "operationId": "network_feature_configs_create",
        "description": cleandoc("""
            Create a new `NetworkFeatureConfig` for a `NetworkService`.
        """),
        "tags": ["network-feature-configs"],
        "requestBody": {
            "description": "NetworkFeatureConfig",
            "required": True,
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": "#/components/schemas/RttNetworkFeatureConfig",
                    },
                },
            },
        },
        "responses": {
            **response("201", EXT_NFC_CREATE_SUCCESS),
        },
    },
}

EXT_PATHS = {
    "/network-services/{id}/rtts": EXT_NS_STATISTICS_RTT_READ,
    "/network-features": EXT_NF_LIST,
    "/network-features/{id}": EXT_NF_READ,
    "/network-feature-configs": EXT_NFC_COLLECTION,
}


SPEC = {
    "openapi": "3.0.0",
    "info": {
        "version": "1.0.0",
        "title": "IX-API: RTT Statistics",
        "description": cleandoc(EXT_DESCRIPTION),
        "contact": {
            "url": "https://ix-api.net",
        },
        "license": {
            "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
            "name": "Apache 2.0",
        }
    },
    "paths": EXT_PATHS,
    "components": EXT_COMPONENTS,
}


def generate_json():
    """Render JSON schema"""
    return json.dumps(SPEC)


if __name__ == "__main__":
    print(generate_json())
